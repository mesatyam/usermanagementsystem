package com.usermanagement.controller;


import com.usermanagement.models.FunzoneModel;
import com.usermanagement.models.UserModel;
import com.usermanagement.repository.FunzoneRepository;
import com.usermanagement.repository.UserRepository;
import com.usermanagement.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RestController
@RequestMapping("/funzone")
public class FunzoneController {

    @Autowired
    private FunzoneRepository funzoneRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private JwtUtil jwtUtil;



    @GetMapping
    public List<FunzoneModel> getAll(){
        return funzoneRepository.findAll();
    }

    @GetMapping("/{name}")
    public FunzoneModel getByName(@PathVariable String name){
        return funzoneRepository.getByFunzoneName(name);
    }

    @GetMapping("/{name}/admin")
    public String getAdminName(@PathVariable String name){
        return funzoneRepository.getByFunzoneName(name).getAdmin();
    }

    @PutMapping("/update")
    public String updateFunzone(@RequestBody FunzoneModel funzoneModel) {
        funzoneRepository.save(funzoneModel);
        return "Funzone Updated";
    }

    @PostMapping("/add")
    public String addFunzone(@RequestBody FunzoneModel funzoneModel, HttpServletRequest request){
        UserModel user = extractUserfromJWT(request);
        funzoneModel.setAdmin(user.getEmail());
        funzoneModel.getMembersList().add(user.getEmail());

        funzoneRepository.insert(funzoneModel);
        return funzoneModel.getFunzoneName() + " Successfully Created";
    }

    @PostMapping("/{funzonename}/invite")
    public String inviteMembers(@PathVariable String funzonename,HttpServletRequest request, @RequestBody List<String> memberlist){
        UserModel user = extractUserfromJWT(request);
        FunzoneModel funzoneModel = funzoneRepository.getByFunzoneName(funzonename);
        if(funzoneModel.getMembersList().contains(user.getEmail())){

            funzoneModel.addNewMembers(memberlist);
            funzoneRepository.save(funzoneModel);
            return "Succesfully Invited People";
        }
        else
            return "You are not part of this group";

    }
    @DeleteMapping("/{funzonename}/deletemember")
    public String deleteMember(@PathVariable String funzonename, @RequestBody List<String> member,HttpServletRequest request){
        UserModel user = extractUserfromJWT(request);
        FunzoneModel funzoneModel = funzoneRepository.getByFunzoneName(funzonename);
        if(funzoneModel.getAdmin().equals(user.getEmail())){
            if(member.contains(user.getEmail())){
                funzoneModel.getMembersList().removeAll(member);
                funzoneModel.setAdmin(funzoneModel.getMembersList().get(0));
                funzoneRepository.save(funzoneModel);
                return "Removed Members\nChanged admin";
            }
            else {
                funzoneModel.getMembersList().removeAll(member);

                funzoneRepository.save(funzoneModel);
                return "Removed Members";
            }
        }
        else {
            if(member.contains(user.getEmail()) && member.size()==1){
                funzoneModel.getMembersList().remove(user.getEmail());

                funzoneRepository.save(funzoneModel);
                return "Removed self successfully";
            }
            else {
                return "You can not Remove Members";
            }
        }
    }

    @DeleteMapping("/delete/{id}")
    public String deleteFunzone(@PathVariable String id){
        funzoneRepository.deleteById(id);
        return "Selected Funzone Deleted";
    }

    @DeleteMapping("/delete/all")
    public String deleteAllFunzone() {
        funzoneRepository.deleteAll();
        return "Deleted All Funzone";
    }

    public UserModel extractUserfromJWT(HttpServletRequest request){

        final String authorizationHeader = request.getHeader("Authorization");
        String username = null; String jwt = null;
        if( authorizationHeader != null && authorizationHeader.startsWith("Bearer ")){
            jwt = authorizationHeader.substring(7);
            username = jwtUtil.extractUsername(jwt);
            System.out.println(username);
            return  userRepository.findByEmail(username);
        }
        else  return null;
    }
}
