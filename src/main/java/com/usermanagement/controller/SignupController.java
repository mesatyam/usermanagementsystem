package com.usermanagement.controller;

import com.usermanagement.models.UserModel;
import com.usermanagement.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SignupController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    PasswordEncoder passwordEncoder;

    @PostMapping("/signup")
    public String signup(@RequestBody UserModel user){
        String encryptpass = passwordEncoder.encode(user.getPassword());
        user.setPassword(encryptpass);
        userRepository.insert(user);
        return "Successful Signup \nWelcome, "+user.getFname();
    }
}
