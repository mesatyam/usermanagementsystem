package com.usermanagement.repository;

import com.usermanagement.models.FunzoneModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FunzoneRepository extends MongoRepository<FunzoneModel,String> {
        FunzoneModel getByFunzoneName(String name);
}
