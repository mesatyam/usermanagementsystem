package com.usermanagement.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Document("funzone_db")
public class FunzoneModel {
    @Id
    private String id;
    private String funzoneName;
    private String Admin;
    private List<String> membersList;
    public void addNewMembers(List<String> nm){
        this.membersList.addAll(nm);
    }

}
